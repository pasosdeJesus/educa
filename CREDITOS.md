
Puede ver el listado de personas que han implementado en:
<https://gitlab.com/pasosdeJesus/educa/-/graphs/main>

Imagen de portada:
https://www.publicdomainpictures.net/en/view-image.php?image=239750&picture=notebook-with-pen-and-pencil

Clip de anexos: https://openclipart.org/detail/168413/paper-clip

Base para la foto de usuario predeterminada:
https://openclipart.org/image/400px/15812 

Esta aplicación usa los motores 
[sip](https://github.com/pasosdeJesus/sip),
[mr519_gen](https://github.com/pasosdeJesus/mr519_gen) y
[heb412_gen](https://github.com/pasosdeJesus/heb412_gen)
[cor1440_gen](https://github.com/pasosdeJesus/cor1440_gen)
también mantenidos y re-factorizados por Pasos de Jesús de 
aplicaciones web desarrolladas voluntariamente o financiadas por 
diversas organizaciones que han cedido al dominio público lo desarrollado, 
ver por ejemplo
<https://github.com/pasosdeJesus/sivel2/blob/master/CREDITOS.md>

También usa una pila tecnológica de código abierto que incluye 
PostgreSQL, nginx, Ruby, nodejs y Ruby on Rails, así como muchas
gemas ruby y paquetes npm.

El sistema operativo de desarrollo y objetivo principal para producción
ha sido adJ/OpenBSD, ver https://aprendiendo.pasosdeJesus.org


Agradecimientos a Jesús que es Dios y es camino, verdad y vida (ver Juan 14.6).
