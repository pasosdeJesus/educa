require 'sip/concerns/controllers/hogar_controller'

module Sip
  class HogarController < ApplicationController

    include Sip::Concerns::Controllers::HogarController

    def ppd
      render layout: 'application'
    end


    def gracias
      render layout: 'application'
    end

  end
end
